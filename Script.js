var obj = {
    "avgRatings": 4,
    "ratingsCount": 40,
    "ratingDetails": [{
        "text": "5 Star",
        "avgRatingsPercentage": 85
    },
    {
        "text": "4 Star",
        "avgRatingsPercentage": 9
    },
    {
        "text": "3 Star",
        "avgRatingsPercentage": 4
    },
    {
        "text": "2 Star",
        "avgRatingsPercentage": 2
    }, {
        "text": "1 Star",
        "avgRatingsPercentage": 1
    }


    ]
};

var script = document.createElement('script');
script.src = "https://kit.fontawesome.com/9d091bc651.js";
document.head.appendChild(script);


var maindiv = document.getElementById('ratingstar');
maindiv.style.cssText = " width: 120px;text-align: center;"

//for creating rating stars //
var list = document.createElement('ul'); //creating unorder list elements 
list.style.cssText = "display:flex;list-style:none;margin-left:100px";
list.setAttribute("id", "staricons");  // set the id name to list tag 

function displaystars(listname) {
    for (i = 1; i <= 5; i++) {
        var li = document.createElement('li');
        li.style.cssText = "padding-right:1px"
        li.style.cssText = "list-style:none;margin-right:15px;color:black";
        var span = document.createElement("span");
        span.className = "fas fa-star";
        li.appendChild(span);
        listname.appendChild(li);
    }
}
maindiv.appendChild(list);

displaystars(list); // calling to display stars


//apply the color to  stars  //
function starcolor(starcount, idname) {
    for (var i = 0; i <= starcount - 1; i++) {
        document.getElementById(idname).children[i].style.color = "red";
    }
}
starcolor(4, "staricons");  //how many star u want color and ul id name;



var tooldiv = document.createElement("div");
tooldiv.classList.add("tooltiptext");
tooldiv.style.cssText = "visibility: hidden; width: 120px;text-align: center; border-radius: 6px;padding: 5px 0;";
tooldiv.setAttribute("id", "customerReview");
maindiv.appendChild(tooldiv);

var customerReview = document.getElementById('customerReview');
// onmouse over display the div
document.getElementById("staricons").addEventListener("mouseover", function () {
    customerReview.style.cssText = "visibility: visible;background:white;width:400px;min-height:300px;margin:50px auto;border:1px solid;border-radius:10px;position:absolute;top:-15px;left:22px"
})
// onmouse out hide the div
document.getElementById("staricons").addEventListener("mouseout", function () {
    customerReview.style.cssText = "visibility:hidden;"
})


//  var cssafter=window.getComputedStyle(customerReview);
// console.log(cssafter.style.setProperty("border-color", "yellowgreen"))
var content = document.createElement('div');
content.style.cssText = " display: flex;min-width: 200px;margin-left:18px"

var ratingdata = document.createElement('div');
ratingdata.setAttribute('id', 'boxs')
var ratingResult = document.createElement('span');
ratingResult.innerHTML = `${obj.avgRatings} out of 5`;
ratingResult.style.cssText = "padding-top: 15px;margin-left:10px";
content.appendChild(ratingdata);
content.appendChild(ratingResult);
customerReview.appendChild(content);

var para = document.createElement("p");
para.innerHTML = `${obj.ratingsCount} Customer rating `;
customerReview.appendChild(para);
para.style.cssText = "margin:0px 155px 10px 0px";


// to create rating bars with functon
function showrating(x, y) {
    var maindiv = document.createElement('div');
    var temp = " width: 300px;  margin-left: 60px;  display: flex; justify-content: space-between;margin-bottom: 15px";
    maindiv.style.cssText = temp;

    var span5 = document.createElement("span");
    span5.style.cssText = " color:#3366e9;"
    span5.innerHTML = y;

    maindiv.appendChild(span5);
    var stardiv = document.createElement("div");
    stardiv.style.cssText = "width: 200px;height: 20px;  background:#F5F8FF; border-radius: 10px";

    var fivestar = document.createElement('div');
    fivestar.style.cssText = "height: 20px; background: #FFCC48; border-radius: 10px"
    fivestar.style.width = x + '%';


    var spanp = document.createElement('span');
    spanp.innerHTML = x + '%';
    stardiv.appendChild(fivestar);
    maindiv.appendChild(stardiv);
    maindiv.appendChild(spanp);
    customerReview.appendChild(maindiv);
}
// calling to how may rating bars u want 
for (var i = 0; i <= 4; i++) {
    showrating(obj.ratingDetails[i].avgRatingsPercentage, obj.ratingDetails[i].text);
}

var divres = document.createElement("div");
var h4 = document.createElement('h4');
h4.style.cssText = 'color:#577EE2;'
h4.innerHTML = "How do we calculate ratings?";
h4.style.cssText = "text-align:center;color:#3366e9;"
divres.appendChild(h4);
customerReview.appendChild(divres);

var ratingdatadiv = document.getElementById("boxs");

// to create rating star in inner div which is when we hover on div
var innerlist = document.createElement('ul');
innerlist.setAttribute("id", "innerstaricon"); // set id name to inner list tag 
innerlist.style.cssText = "display:flex;list-style:none";
ratingdatadiv.appendChild(innerlist);

displaystars(innerlist); // call to display stars and parameter is which div u append the ul tag
starcolor(4, "innerstaricon");  //how many star u want color and ul id name;

